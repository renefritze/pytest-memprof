# -*- coding: utf-8 -*-


def test_memory_profiling(testdir):
    """Make sure that pytest accepts our fixture."""

    # create a temporary pytest test module
    testdir.makepyfile(
        """
        def test_memory_consumption():
            import time
            data = list(range(100000))
            time.sleep(1.0)
            len(data)
            del data

        def test_memory_consumption_2():
            import time
            data = list(range(900000))
            time.sleep(1.0)
            len(data)
            del data
    """
    )

    # run pytest with the following cmd args
    result = testdir.runpytest("--memprof-top-n", "0")
    # fnmatch_lines does an assertion internally
    result.stdout.fnmatch_lines(
        [
            "test_memory_profiling.py::test_memory_consumption_2  - ** MB",
            "test_memory_profiling.py::test_memory_consumption    - ** MB",
        ]
    )

    # make sure that that we get a '0' exit code for the testsuite
    assert result.ret == 0

    # run pytest with the following cmd args
    result = testdir.runpytest("--memprof-top-n", "1")

    # fnmatch_lines does an assertion internally
    result.stdout.fnmatch_lines(
        ["test_memory_profiling.py::test_memory_consumption_2  - ** MB"]
    )


def test_help_message(testdir):
    result = testdir.runpytest("--help")
    # fnmatch_lines does an assertion internally
    result.stdout.fnmatch_lines(
        [
            "memprof:",
            "*--memprof-top-n=MEMPROF_TOP_N*",
            "*limit memory reports to top n entries, report all*",
        ]
    )
